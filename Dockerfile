FROM node:20-alpine


RUN mkdir -p /builds/poffey21/typetta/node_modules && chown -R node:node /builds/poffey21/typetta/

WORKDIR /builds/poffey21/typetta/
USER node

#RUN npm ci
RUN npm install --save-dev eslint eslint-formatter-gitlab

LABEL maintainer="poffey21"

